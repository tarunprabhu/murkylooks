# Murkylooks

Dark theme for Openbox based on Clearlooks and Artix-dark. 

# Installation

Copy the Murkylooks directory into the user's theme directory. This can 
be one of the following:

- `$HOME/.local/share/themes`
- `$HOME/.themes`
- `$XDG_DATA_DIR/themes`

To make the theme available to all users, copy the Murkylooks directory to the
system's theme directory. This is usually `/usr/share/themes` or
`/usr/local/share/themes`.
