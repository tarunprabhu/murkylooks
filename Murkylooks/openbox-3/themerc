#  This file is a part of Murkylooks. Murkylooks is a dark theme for Openbox
#  based on Clearlooks and Artix-dark. 
#
#  Copyright (C) 2022  Tarun Prabhu
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# Fonts

*.font: shadow=n
window.active.label.text.font:shadow=y:shadowtint=30:shadowoffset=1
window.inactive.label.text.font:shadow=y:shadowtint=00:shadowoffset=0
menu.items.font:shadow=y:shadowtint=0:shadowoffset=1

# General parameters

border.width: 2
padding.width: 2
padding.height: 2
window.handle.width: 0
window.client.padding.width: 0
window.client.padding.height: 0
menu.overlap: 0
*.justify: center

# Highlights and shadows

*.bg.highlight: 50
*.bg.shadow:    05

window.active.title.bg.highlight: 35
window.active.title.bg.shadow:    05

window.inactive.title.bg.highlight: 30
window.inactive.title.bg.shadow:    05

# Menu settings

menu.border.color: #000000
menu.border.width: 1

menu.title.bg: solid Flat
menu.title.bg.color: #1a1a1a
menu.title.text.color: #808080

menu.items.bg: Flat Solid
menu.items.bg.color: #2d2e2f
menu.items.bg.border.color: #1a1a1a
menu.items.text.color: #eaeaea
menu.items.disabled.text.color: #808080

menu.items.active.bg: Flat Gradient SplitVertical border
menu.items.active.bg.color.splitTo: #588cd0
menu.items.active.bg.color: #6091d2
menu.items.active.bg.colorTo: #5a8cce
menu.items.active.bg.colorTo.splitTo: #4e84ca
menu.items.active.bg.border.color: #4e84ca
menu.items.active.text.color: #ffffff

menu.separator.width: 1
menu.separator.padding.width: 2
menu.separator.padding.height: 4
menu.separator.color: #404040

# Focused windows

window.active.border.color: #99ccff
window.active.title.separator.color: #282828

window.active.title.bg: Raised Gradient SplitVertical
window.active.title.bg.color.splitTo: #2a2a2a
window.active.title.bg.color: #2d2d2d
window.active.title.bg.colorTo: #303030
window.active.title.bg.colorTo.splitTo: #333333

window.active.label.bg: ParentRelative
window.active.label.text.color: #eaeaea

window.active.button.*.bg: Flat Gradient SplitVertical Border

window.active.button.*.bg.color.splitTo: #585858
window.active.button.*.bg.color: #5d5d5d
window.active.button.*.bg.colorTo: #626262
window.active.button.*.bg.colorTo.splitTo: #686868
window.active.button.*.bg.border.color: #000000
window.active.button.*.image.color: #eaeaea

# Unfocused windows

window.inactive.border.color: #202020
window.inactive.title.separator.color: #383838

window.inactive.title.bg: Raised Flat
window.inactive.title.bg.color: #484848

window.inactive.label.bg: ParentRelative
window.inactive.label.text.color: #808080

window.inactive.button.*.bg: Flat Border
window.inactive.button.*.bg.color: #686868
window.inactive.button.*.bg.border.color: #282828
window.inactive.button.*.image.color: #c8c8c8

# On-screen dislpays (window cycling, desktop switching etc.)

osd.border.width: 1
osd.border.color: #282828

osd.bg: Flat Border Gradient SplitVertical
osd.bg.color.splitto: #363636
osd.bg.color: #333333
osd.bg.colorTo: #303030
osd.bg.colorTo.splitto: #2d2d2d
osd.bg.border.color: #282828

# Window cycling

osd.active.label.bg: ParentRelative
osd.active.label.text.color: #dcdcdc

# Desktop cycling

osd.hilight.bg: Flat Vertical Gradient 
osd.hilight.bg.color: #9ebde5
osd.hilight.bg.colorTo: #749dcf

osd.unhilight.bg: Flat Vertical Gradient
osd.unhilight.bg.color: #BABDB6
osd.unhilight.bg.colorTo: #efefef
